# HereIAm

The script `client.sh` sends its ip to `server.php` through a post parameter
when it changes.
`server.php` then keeps track of the client ip by saving it in
`/var/opt/hereiam-ip/`. Use `GET` to retrieve the ip or `POST` to update it.
